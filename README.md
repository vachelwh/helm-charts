# helm-charts

## 使用文档

### 1.添加仓库地址
```
helm repo add servicea https://gitlab.com/vachelwh/helm-charts/-/raw/main
```

### 2.验证仓库列表
```
helm repo list

NAME    	URL
servicea	https://gitlab.com/vachelwh/helm-charts/-/raw/main
```

### 3.搜索服务
```
helm search repo servicea

AME             	CHART VERSION	APP VERSION	DESCRIPTION
servicea/servicea	0.1.0        	1.0        	Service A
```

### 4.安装服务
```
helm install servicea servicea/servicea
```

### 5.验证安装
```
kubectl get deployment | grep servicea

servicea   1/1     1            1           13m

kubectl get service | grep servicea

servicea   ClusterIP   10.0.255.173   <none>        8888/TCP   13m
```